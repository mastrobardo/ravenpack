import React, { useState, useEffect } from "react";
import PizzaMenu from "./../components/PizzaMenu/PizzaMenu";

const App = _ => {
  const [pizzas, setPizzas] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const url = process.env.API_URL;
  useEffect(() => {
    setLoading(true);
    //while i prefer async/await, it often requires a polyfill when targeting browsers
    //better stick with promises in the client
    fetch(process.env.API_URL)
      .then(res => {
        return res.clone().json();
      })
      .then(res => {
        setPizzas(res);
        setLoading(false);
        return;
      })
      .catch(error => {
         return setError(error)
      });
  }, []); //componentDidMount

  if (!loading) return <PizzaMenu pizzas={pizzas} />;
  if(error) return <p>Hard Pizza times, error: {error}...</p>;
  return <p>Mixing pizza base...</p>;
};

export default App;
