import React from "react";
import PizzaMenu from "../PizzaMenu/PizzaMenu";
import renderer from "react-test-renderer";

const pizzas = [
  { name: "a", image_url: "b", votes: 0 },
  { name: "c", image_url: "d", votes: 0 }
];

it("renders correctly", () => {
  const tree = renderer.create(<PizzaMenu pizzas={pizzas} />).toJSON();
  expect(tree).toMatchSnapshot();
});
