import React, { useState } from "react";
import PizzaCard from "../PizzaCard/PizzaCard";
import "./PizzaMenu.scss";

const pizzaVote =  (name, setVote, updateTree) => {
let url = process.env.VOTE_URL;
  return async _ => {
    let res = await fetch(url, {
        method: 'POST',
        headers: {'Content-Type':'application/x-www-form-urlencoded'}, // this line is important, if this content-type is not set it wont work
        body:`name=${name}`
    })
    let result = res.clone().json().then(pizzas => {
      setVote(true)
      updateTree(pizzas)
      return pizzas
    })
    return result; 
  };
};

const PizzaMenu = props => {
 const [vote, setVote] = useState(false);
 const [pizzas, setPizzas] = useState(props.pizzas);
  return (
    <div className="pizza-container">
      {Object.values(pizzas).map((ele, i) => {
        return (
          <PizzaCard
            enabled={!vote}
            key={i}
            pizza={ele}
            onClick={pizzaVote(ele.name, setVote, setPizzas)}
          />
        );
      })}
    </div>
  );
};

export default PizzaMenu;
