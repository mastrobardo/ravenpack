import React from "react";
import Button from "@material-ui/core/Button";
import Typography from '@material-ui/core/Typography';

const PizzaVoteButton = props => {
  const { enabled, votes, onClick } = props;
  if (enabled) {
    return (
      <Button size="small" color="primary" onClick={onClick}>
        Vote for this pizza!
      </Button>
    );
  } else {
    return(<Typography variant="caption" display="block" gutterBottom>
      This pizza was voted {votes} X times.
    </Typography>);
    // return <span className=""></span>;
  }
};

export default PizzaVoteButton;
