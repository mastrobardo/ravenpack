import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";

import PizzaVoteButton from '../../components/PizzaVoteButton/PizzaVoteButton'

const useStyles = makeStyles({
  card: {
    maxWidth: 345
  },
  media: {
    height: 140
  }
});

const votePizza = name => {};

const PizzaCard = props => {
  const { pizza, 
          onClick,
          enabled } = props;

  const classes = useStyles();
  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={pizza.image_url}
          title={pizza.name}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {pizza.name}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
      <PizzaVoteButton onClick={onClick} enabled={enabled} votes={pizza.votes} />
        
      </CardActions>
    </Card>
  );
};

export default PizzaCard;
