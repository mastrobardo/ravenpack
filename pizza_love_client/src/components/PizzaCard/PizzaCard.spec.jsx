import React from "react";
import PizzaCard from "../PizzaCard/PizzaCard";
import renderer from "react-test-renderer";

it("renders correctly", () => {
  const tree = renderer
    .create(
      <PizzaCard
        enabled={true}
        key={1}
        pizza={{name:'m', image_url: '/static/images/pizza', votes: 0}}
        onClick={()=>{}}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
