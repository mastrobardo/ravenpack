from flask import Blueprint, render_template, request, redirect, url_for, make_response, escape, jsonify
from flask_login import login_required, logout_user, current_user, login_user
from pizza_love import db, app, login_manager
from pizza_love.models.User import User
from pizza_love.models.Pizza import Pizza, pizza_schema
from pizza_love.routes_utils import make_template_response;

routes = Blueprint('routes', __name__)

@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))

def getFromForm(request, prop):
    # really basic, should be done in some more robust way 
    return escape(request.form.get(prop))

@routes.route('/', methods=['GET'])
def index():
    return render_template('login.html')

@routes.route('/login', methods=['GET', 'POST'])
def login():
    if request.method=='POST':
        name = getFromForm(request, 'name')
        password = getFromForm(request, 'password')
        user = User.query.filter_by(name=name, password=password).first()
        print(user,name, password, ';from login')
        if  user:
            login_user(user)
            return redirect(url_for('routes.pizza'))
        else:
            return  make_template_response('register.html', name)
    else:
        return make_template_response('login.html')
        
                           
@routes.route('/register', methods=['GET', 'POST'])
def signup():
    if request.method=='GET':
        return make_template_response('register.html')
    else: 
        print(request)
        name = getFromForm(request, 'name')
        password = getFromForm(request, 'password')
        email = getFromForm(request, 'email')
        user = User.query.filter_by(email=email).first()
        print(user)
        if user:
            return redirect(url_for('routes.pizza'))
        user = User(name=name,password=password, email=email)
        try:
            db.session.add(user) 
            db.session.commit()
        except Exception as e: 
            print(e)
            raise
        else:
            return redirect(url_for('routes.login'))


@routes.route('/pizza', methods=['GET', 'POST', 'PUT'])
@login_required
def pizza():
    name = request.form.get('name')
    image_url = request.form.get('image_url')
    
    if request.method=='GET':
        return make_response(render_template('vote.html'))

    elif request.method=='PUT':
        piz = Pizza.query.filter_by(name = name).first()
        if piz:
            print('{piz} exists')
            return make_response('error {piz} exists')
        piz = Pizza(name=name,image_url=image_url,votes=0)
    else:
        piz = Pizza.query.filter_by(name = name).first()
        piz.votes += 1
    try:
        db.session.add(piz) 
        db.session.commit()
    except:
        print( 'There was an error')
        raise
    else:
        print('created')
     
    return make_response(jsonpizza())


@routes.route('/jsonpizza', methods=['GET'])
def jsonpizza():
    # name = escape(request.args.get('name'))
    # if name != None:
    #     print(name == False)
    #     response = Pizza.query.filter_by(name=name)
    # else:
    response = Pizza.query.all()
    
    # print( jsonify(pizzas=list(Pizza.query.all())))
    print(pizza_schema.dump(response))
    return make_response(jsonify(pizza_schema.dump(response)))
  
  