from pizza_love import db

class User(db.Model):
    __tablename__ = 'User'
    id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))
    email = db.Column('email',db.String(50),unique=True , index=True)
    
    def __init__(self, name, password, email):
        self.name = name
        self.password = password
        self.email = email

    def is_authenticated(self):
        return True
 
    def is_active(self):
        return True
 
    def is_anonymous(self):
        return False
 
    def get_id(self):
        return str(self.id)

    def __repr__(self):
        return '<User %r>' % self.name