from pizza_love import db, ma

class Pizza(db.Model):
    __tablename__ = 'Pizza'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    image_url = db.Column(db.String(50))
    # recipe = db.Column(db.String(50))
    votes = db.Column(db.Integer, default=0)

    def to_dict(self):
        return dict([(k, getattr(self, k)) for k in self.__dict__.keys() if not k.startswith("_")])

    def __init__(self, name, image_url, votes):
        self.name = name
        self.votes = votes
    
    def __repr__(self):
        return '<Pizza %r>' % self.name

class PizzaSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ("name", "image_url", "votes")

pizza_schema = PizzaSchema(many=True)