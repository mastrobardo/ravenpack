from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from flask_marshmallow import Marshmallow
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
app.config['SECRET_KEY'] = os.urandom(24)
engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
app.config["SQLALCHEMY_ECHO"] = True
db = SQLAlchemy()
# db.connections.close_all()
db.init_app(app)
ma = Marshmallow(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'routes.login'

from .routes import routes as routes_blueprint
app.register_blueprint(routes_blueprint)

print(routes_blueprint.register)

from pizza_love import models