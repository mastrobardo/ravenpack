#!/bin/bash

declare -A pizza0=(
    [name]='margherita'
    [image_url]='static/images/pizza.png'
    [votes]=6
)
declare -A pizza1=(    
    [name]='diavola'
    [image_url]='static/images/pizza.png'
    [votes]=9
)
declare -A pizza2=(
    [name]='marinara'
    [image_url]='static/images/pizza.png'
    [votes]=1
)
declare -A pizza3=(    
    [name]='romana'
    [image_url]='static/images/pizza.png'
    [votes]=0
)

declare -n pizza
for pizza in ${!pizza@}; do
    sleep 1
    sqlite3 pizza_love/app.db "INSERT INTO Pizza('name','image_url', 'votes') VALUES ('${pizza[name]}', '${pizza[image_url]}',${pizza[votes]})"
    # " WHERE NOT EXISTS (SELECT * FROM Pizza WHERE name = ${pizza[name]})"
done

