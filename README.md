**_HOW TO RUN_**

```cd ravenpack```

install (create an env before)
```pip install -r requirements.txt```

create db
```sqlite3 pizza_love/app.db```    

migrate   
```
python migrate.py db init    
python migrate.py db migrate   
python migrate.py db upgrade    
```

seed
```./seed.sh```    

move to pizza_love_client
```
cd/pizza_love_client
```

In pizza_love_client, run:
```npm i```
```npm run build:dev```

run
```./run.sh```     

Hit:
```http://127.0.0.1:5000/login```

It is my first attempt with Flask, and im not no used to python, so might have done some mistake here and there on the flask part.
i tried to stick with common templating approach for login / register ( just to show i understand what templates and redirects in flask are),
using flask_login. ( Would have been fairly easier just to have a React spa, but at least i grasped the system like this)

FE tests are pretty basic ( just snapsots).
I use material design for the React frontend ( /pizza ). I love stateless components, but proabably a PureComponent would have been better for PizzaMenu,
in a real-life situation.


Only Mobile view! (up to Iphone X width). I commented out the serviceWorker's setup in vote.html, enabling it should make the app installable ( disabled cause of cache )