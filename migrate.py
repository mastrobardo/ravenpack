from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from pizza_love import  db, app
from pizza_love.models import *

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///pizza_love/app.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)
# manager.add_command('db', seed)

@manager.command
def seed():
    list = ['margherita', 'romana', 'marinara', 'diavola'] 
    for i in list: 
        print(i)   
        pizza = Pizza(i,0)
        # pizza.save()
        # print(pizza)   
        # db.session.add(pizza)
    
    db.session.commit()

if __name__ == '__main__':
    manager.run()