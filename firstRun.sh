#!/bin/bash

# sqlite3 -batch pizza_love/app.db
python -m venv dev
source dev/bin/activate
pip install -r requirements.txt
export FLASH_APP=pizza_love
export FLASK_DEBUG=1
flask run
# source seed.sh